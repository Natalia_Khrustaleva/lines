
#include "stdlib.h"
#include "string"
#include "windows.h"
#include "resource.h"
#include "random.h"
#include <list>
#include <queue>
#include <stack>
#include <strstream>
#include <Fcntl.h>
#include<SYS\types.h>
#include<SYS\stat.h> 
#include <Io.h>
using namespace std;

#define MENU_HEIGHT 45

#define TOP_HEIGHT 46	// ������ ����� (��� ������������ ����) � ��������
#define CELL_SIZE 45	//������ ������� ������ � �������� (��� ��� ����������)

#define N1 4	//���������� �������� ��� ��������� ������
#define N2 12	//���������� �������� ��� ������ ������
#define N3 9	//���������� �������� ��� �������� ������

#define MAX_MAP_X 20	//������������ ������ ���� �� x
#define MAX_MAP_Y 12    //������������ ������ ���� �� y


struct	
{

	//��������� �������� ��� ���� ������
	int y;
	//���� ������ � ���� ������
	int color;
	//���� ������-��������� � ���� ������
	//(����� ������� ����� �������� ����� ��� ������, ����� color != pre_color)
	int pre_color;
	//����� ��������, ��������� � ������� ������, ��� ���������, ��������, ������ ������
	int num_pic;
} 

// ������� ����
map[MAX_MAP_X][MAX_MAP_Y];	

// ���� �������� ������
int ball_color;


struct info
{
	int score;
	int time;
	char name[30];
}
// ������ ����������
leaders[3];

// ����������� ���������, ����� ������� � �������
const info null_leader={10,3600,"noname"};

// ��������� �������� ���������� �����
int y_lines;

// ����� ����
int gametime;
// ����
int gamescore;
// ��� ����: 0-easy, 1-normal, 2-hard, 3-custom
int gametype;

// ������� ������ ����
int max_x; // �� x
int max_y; // �� y
//���������� ������������ �����
int app_balls;
//���������� ��������� �����
int del_balls;
//��������� ������������� ��������� �������
int joker=0;


#define LINESlog //�������� �����������

#if defined LINESlog
FILE *log;

const char* aCell_states[6] = 
{
  "������ �������� ���� �����",
  "� ������ �������� ���� ��������� ��������� ���������� ����",
  "� ������ �������� ���� ���������� �������������� ����",
  "� ������ �������� ���� �������� ���",
  "����������� � ������ �������� ���� ��� ������������",
  "� ������ �������� ���� ���������� ���������������� ����"
};

const char* aLines_states[5] = 
{
  "������� �������������� ����� �������� ����",
  "������� �������������� ����� ������ ���������� �������� ����",
  "������� ������������ ����������� �������� ����",
  "������� ������������ �������� ����� �����",
  "������� ������������ ��������� ����� ����� �� ����"
};
const char* aBall_color[8] = 
{
    "����������",
    "�������",
    "������",
    "�������",
    "�������",
    "�����",
    "�������",
    "���������"
};

#endif


HANDLE bmp_0,bmp_prestand, bmp_stand, bmp_jump[N2], bmp_explode[N3], bmp_appear[N1], bmp_numbers, bmp_points;
HANDLE *bmp[6];

HDC hDC;
HDC hCompatibleDC;
HWND hWnd;
HINSTANCE hInst;
TCHAR szTitle[] = "Lines";
TCHAR szWindowClass[] = "LINES";

RECT clRect;

// ����� "������"
class Cell
{
public:
	int posx; // ������� ������ �� ���� (����� ������� �� 0)
	int posy; // ������� ������ �� ���� (������ ���� �� 0)
	bool operator ==(const Cell & b) const
	{
        return (b.posx == posx && b.posy == posy);
    }
	bool operator !=(const Cell & b) const
	{
        return (!(*this == b));
    }
	int & State() const
	{
        return map[posx][posy].y;
    }
	int & Color() const
	{
        return map[posx][posy].color;
    }
	int & PreColor() const
	{
        return map[posx][posy].pre_color;
    }
	int & NumPic() const
	{
        return map[posx][posy].num_pic;
    }

	// ������� "������"
	void ACell(int e) const
	{
		int &y=State();

		#if defined LINESlog
		int y_old = y;
		#endif

		switch(y)
		{
		// ���������� ����
		case 0:
			if (e==1)	
            {
                Moving_ball();
                y=3;
            }
			if (e==2 && Need_help())
            {
                Create_help_again();	
                y=1;
            }
			else if (e==2)	
            {
                Create_help();
                //MessageBox(NULL, "EXAMPLE!!!", "create help", MB_OK);
                y=1;
            }
			break;
		// ���������
		case 1:
			if (e==1)		
            {
                Free_cell();
                y=3;
            }
			if (e==3)
                y=2;
			break;
		// ��������� ����
		case 2:
			if (e==3 && Last_appear())
            {
                Show_first_img();
                y=3;
            }
			else if (e==3)	
            {
                Show_next_img();
            }
			break;
		// ��� �����
		case 3:
			if (e==0 && Need_help() )
            {
                Create_help_again();
                y=1;
            }
			else if (e==0)	
            {
                Moving_ball_with_help();
                y=0;
            }
			if (e==4) 
                y=5;
			if (e==5)
                y=4;
			break;
		// ��� �������
		case 4:
			if (e==0)		
            {
                Show_first_img();
                Moving_ball_with_help();
                y=0;
            }
			if (e==6)
            {
                Show_first_img();
                y=3;
            }
			if (e==5 && Last_jump())	
            {
                Show_first_img();
            }
			else if (e==5)	
            {
                Show_next_img();
            }
			break;
		// �������� ����
		case 5:
			if (e==4 && Last_del() && Need_help())	
            {
                Show_first_img();
                Create_help_again();
                y=1;
            }
			else if (e==4 && Last_del())
            {
                Show_first_img();
                Moving_ball_with_help();
                y=0;
            }
			else if (e==4)			
            {
                Show_next_img();
            }
			break;
		}

		#if defined LINESlog
		if (y!=y_old)
		{
			char s[30];
			time_t t;
			time(&t);
			strftime(s,30,"%X", gmtime(&t));
            fprintf(log,"[%s] => ��� ������ (%d,%d)\n ��������� ������� �� ��������� \"%s\"\n � ��������� \"%s\"\n ���� ���� � ������ \"%s\".\n\n\n",s,posx+1,posy+1,aCell_states[y_old],aCell_states[y], aBall_color[map[posx][posy].color]);
		}
		#endif

		
		DrawState();
	}

	void DrawState() const
	{
		SelectObject(hCompatibleDC, bmp[State()][NumPic()]);
		BitBlt(hDC,posx*CELL_SIZE, TOP_HEIGHT+posy*CELL_SIZE, CELL_SIZE+1, CELL_SIZE+1, hCompatibleDC, Color()*CELL_SIZE, 0, SRCCOPY);
	}

private:
	//�������� ����������
	
	//��������� �������������� ���������
	bool Need_help() const
	{
        return PreColor()!=-1;
    }
	//��������� ������ ��������� ����
	bool Last_appear() const
	{
        return (NumPic()==N1-1);
    }
	//��������� ������ ������ ��� ����
	bool Last_jump() const
	{
        return (NumPic()==N2-1);
    }
	//��������� ������ �������� ����
	bool Last_del() const
	{
        return (NumPic()==N3-1);
    }
	
	//��������� �����������

	//���������� � ������ ����������� ���
	void Moving_ball() const
	{
        Color()=ball_color;
    }
	//������������ ���������
	void Create_help() const
	{
        if (joker==0)
        {
            Color()=random(7);
        }
        else if(joker>0)
        {
            Color()=7;//��������, ��������� �� 7 ��� ���������� ����������� ��� �������
            joker--;
        }
    }
	//�������� ������
	void Free_cell() const
	{
        PreColor()=Color();
        Color()=ball_color;
    }
	//���������� � ������ � ���������� ����������� ���
	void Moving_ball_with_help() const
	{
        Color()=0;
    }
	//������������ ���������
	void Create_help_again() const
	{
        Color()=PreColor();
        PreColor()=-1;
    }
	//������� �� ����� ��������� �������� ��� �������� ���������
	void Show_next_img() const
	{
        ++NumPic();
    }
	//������� �� ����� ������ �������� ��� �������� ���������
	void Show_first_img() const
	{
        NumPic()=0;
    }

};

//������� ���
Cell ball;
//��������� ��� (������ ������)
Cell click_ball;

//������ ������������ �����
std::list<Cell> appear_list;
//������ ����������� �����
std::list<Cell> explode_list;
//���� �� �������� �������� ����������� ���
std::stack<Cell> path;

std::list<Cell>::iterator itr;


// ��������� �������, ����������� ����� � ���������, ��������� �������, ����� ����� ��� ������� �������� �� ���� �� ������
void ALines(int);//
void Start_jumping();//������ �������
void Start_moving();//������ �������
void End_jumping();//��������� �������
void Jumping();//�������
void Move_next();//����������� ��� �� ��������� ������
void End_moving();//��������� ������������
void Start_delete();//������ ��������
void Start_appearing();//������ ���������
void Deleting();//�������
void End_delete();//��������� ��������
void Appearing();//���������
void End_appear();//��������� ���������

bool Empty_cell();//��������� ������ �����
bool Cell_with_ball();//� ��������� ������ ����� ���
bool Cell_with_jumping_ball();//� ��������� ������ ������� ���
bool Have_path();//���� ���� �� �������� ������ �� ���������
bool Moving_finished();//������������ ���������
bool Del_line_after_moving();//����� ������������ ���������� ������� �����
bool Del_finished();//�������� ���������
bool Appear_finished();//��������� ���������
bool Del_line_after_appearing();//����� ��������� ����� ���������� ������� �����

bool FindEmptyCell(Cell &);
void GenerateAppearList();//������� ������ ������������ �����
void CheckAppearList();
bool FindPath(const Cell &, const Cell &);//����� ���� �� ������ from � ������ in
bool CheckLines(const Cell &);//��������� ������������ �����
bool Valid(const Cell &);

void GameOver();
void NewGame();
void DrawTime();
void DrawScore();
void DrawTop();

void CheckCustomParameters();//��������, ��� ������������ ���� ��������� �� ����������� ���������
void GetInfo();
void WriteInfo();


ATOM				My_Class(HINSTANCE);
BOOL				InitInstance(HINSTANCE,int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	Custom(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	BestResults(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	GetName(HWND, UINT, WPARAM, LPARAM);



//������� ������� ����
int APIENTRY WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine, int nCmdShow)
{
	MSG msg;
	HACCEL hAccelTable;

	randomize();

	hInst=hInstance;
	My_Class(hInstance);

	if (!InitInstance (hInstance,nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_LINES);

	//������� ���� ��������� Windows
	while (GetMessage(&msg, NULL, 0, 0)) 
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) 
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return msg.wParam;
}

//����������� ������ ����
ATOM My_Class(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX); 

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC)WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInst, (LPCTSTR)IDI_ICON1);
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= (LPCSTR)IDC_LINES;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_ICON1);

	return RegisterClassEx(&wcex);
}

// �������� � ����������� ����
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hWnd = CreateWindow(szWindowClass, szTitle, 
	   WS_OVERLAPPED|WS_CAPTION|WS_SYSMENU|WS_MINIMIZEBOX,// WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

// ������� ������� ����
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static PAINTSTRUCT ps;
	static RECT Rect;
	static HMENU hMenu;
	static HKEY hKey;
	static Cell l;
	static int temp;
	static	FILE *f;

	switch (message) 
	{
// ��������� ��������� ��� �������� ����
        /*��������� WM_CREATE ���������� �����, ����� ��������� �����������, ������� ����� ������� CreateWindowEx ��� CreateWindow ������ ���� ������� ����. ������� ��������� ������ ���� ��������� ��� ��������� ����� ����, ��� ���� �������, �� �� ����, ��� ���� ���������� �������. ��������� ���������� ����� ������������ �������� �������� CreateWindowEx ��� CreateWindow.

���������

WM_CREATE  
lpcs = (LPCREATESTRUCT) lParam; // ��������� � ������� ��������
*/
	case WM_CREATE: 
		randomize();

		bmp[0] = &bmp_0;
		bmp[1] = &bmp_prestand;
		bmp[2] = &bmp_appear[0];
		bmp[3] = &bmp_stand;
		bmp[4] = &bmp_jump[0];
		bmp[5] = &bmp_explode[0];

		bmp_0 = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_0));
		bmp_prestand = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_PRESTAND));

		bmp_appear[0] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_APPEAR_1));
		bmp_appear[1] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_APPEAR_2));
		bmp_appear[2] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_APPEAR_3));
		bmp_appear[3] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_APPEAR_4));

		bmp_stand = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_STAND));

		bmp_jump[0] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_JUMP_3));
		bmp_jump[1] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_JUMP_2));
		bmp_jump[2] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_JUMP_1));
		bmp_jump[3] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_JUMP_2));
		bmp_jump[4] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_JUMP_3));
		bmp_jump[5] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_STAND));
		bmp_jump[6] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_JUMP_4));
		bmp_jump[7] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_JUMP_5));
		bmp_jump[8] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_JUMP_6));
		bmp_jump[9] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_JUMP_5));
		bmp_jump[10] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_JUMP_4));
		bmp_jump[11] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_STAND));

		bmp_explode[0] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_EXPLODE_1));
		bmp_explode[1] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_EXPLODE_2));
		bmp_explode[2] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_EXPLODE_3));
		bmp_explode[3] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_EXPLODE_4));
		bmp_explode[4] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_EXPLODE_5));
		bmp_explode[5] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_EXPLODE_6));
		bmp_explode[6] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_EXPLODE_7));
		bmp_explode[7] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_EXPLODE_8));
		bmp_explode[8] = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_EXPLODE_9));

		bmp_numbers = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_NUMBERS));
		bmp_points = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_POINTS));
        
		hDC = GetDC(hWnd);
		hCompatibleDC = CreateCompatibleDC(hDC);

		#if defined LINESlog
		log = fopen("lines.log","wt");
		#endif

		GetInfo();
		hMenu = GetSubMenu(GetMenu(hWnd),0);
		CheckMenuItem(hMenu, gametype+IDM_EASY, MF_CHECKED);
		GetWindowRect(hWnd,&Rect);
		MoveWindow(hWnd,Rect.left,Rect.top,CELL_SIZE*max_x+7,TOP_HEIGHT+CELL_SIZE*max_y+MENU_HEIGHT,TRUE);
		NewGame();

		SetTimer(hWnd,0,1000,NULL);
		break;

// ��������� ��������� �� ������� ����� ������ ����
	case WM_LBUTTONDOWN:
		click_ball.posx = LOWORD(lParam)/CELL_SIZE;
		click_ball.posy = (HIWORD(lParam)-TOP_HEIGHT)/45;
		ALines(0);
		break;

// ��������� ��������� �� �������
	case WM_TIMER:
		switch (LOWORD(wParam))
		{
		case 0:
			gametime++;
			DrawTime();
			break;
		case 1:
			ALines(1);
			break;
		}
		break;

// ��������� ��������� WM_COMMAND
        /*��������� WM_COMMAND ���������� �����:

    ������������ ����� ������ ����
    ������� ���������� �������� �������������� ��������� ������������� ����
    ���������� ������� ������� ������������. 

��������� ����� ��������� ������������ � ������� ������� ����.

Wparam - ���������� �������� ���������: ������� ���������� ��� �����������.

Lparam - ������������� ��������, ���� ��� �� �����������.

����� ��������� ����� ��������� ���������� ������� 0.*/
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDM_EASY:
			max_x=9;
			max_y=6;
			app_balls = 2;
			del_balls = 4;
			break;
		case IDM_NORMAL:
			max_x=9;
			max_y=9;
			app_balls = 3;
			del_balls = 5;
			break;
		case IDM_HARD:
			max_x=20;
			max_y=12;
			app_balls = 10;
			del_balls = 4;
			break;
		case IDM_CUSTOM:
			DialogBox(hInst, (LPCTSTR)IDD_CUSTOMBOX, hWnd, (DLGPROC)Custom);
			CheckCustomParameters();
			break;
		}
		switch (LOWORD(wParam))
		{
		case IDM_ABOUT:
			DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
			break;
        case ID_ABOUT_HELP:
            DialogBox(hInst, (LPCTSTR)IDD_About_lines, hWnd, (DLGPROC)About);
            break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case IDM_EASY:
		case IDM_NORMAL:
		case IDM_HARD:
		case IDM_CUSTOM:
			CheckMenuItem(hMenu, gametype+IDM_EASY, MF_UNCHECKED);
			CheckMenuItem(hMenu,LOWORD(wParam), MF_CHECKED);
			gametype=LOWORD(wParam)-IDM_EASY;
			GetWindowRect(hWnd,&Rect);
			MoveWindow(hWnd,Rect.left,Rect.top,CELL_SIZE*max_x+7,TOP_HEIGHT+CELL_SIZE*max_y+MENU_HEIGHT,TRUE);
		case IDM_NEW:
			NewGame();
			InvalidateRect(hWnd,NULL,FALSE);
			break;
		case IDM_BESTRESULTS:
			DialogBox(hInst, (LPCTSTR)IDD_BESTRESULTSBOX, hWnd, (DLGPROC)BestResults);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);

		}
		break;
// ��������� ��������� ��� ��������� ����
	case WM_PAINT:
		BeginPaint(hWnd, &ps);

		DrawTop();
		for (l.posx=0;l.posx<max_x;l.posx++)
		for(l.posy=0;l.posy<max_y;l.posy++)
			l.DrawState();

		EndPaint(hWnd, &ps);
		break;
// ��������� ��������� ��� ����������� ����
	case WM_DESTROY:
		DeleteObject(bmp_0);
		DeleteObject(bmp_prestand);
		DeleteObject(bmp_appear[0]);
		DeleteObject(bmp_appear[1]);
		DeleteObject(bmp_appear[2]);
		DeleteObject(bmp_appear[3]);
		DeleteObject(bmp_stand);
		DeleteObject(bmp_jump[0]);
		DeleteObject(bmp_jump[1]);
		DeleteObject(bmp_jump[2]);
		DeleteObject(bmp_jump[3]);
		DeleteObject(bmp_jump[4]);
		DeleteObject(bmp_jump[5]);
		DeleteObject(bmp_jump[6]);
		DeleteObject(bmp_jump[7]);
		DeleteObject(bmp_jump[8]);
		DeleteObject(bmp_jump[9]);
		DeleteObject(bmp_jump[10]);
		DeleteObject(bmp_jump[11]);
		DeleteObject(bmp_explode[0]);
		DeleteObject(bmp_explode[1]);
		DeleteObject(bmp_explode[2]);
		DeleteObject(bmp_explode[3]);
		DeleteObject(bmp_explode[4]);
		DeleteObject(bmp_explode[5]);
		DeleteObject(bmp_explode[6]);
		DeleteObject(bmp_explode[7]);
		DeleteObject(bmp_explode[8]);
		DeleteObject(bmp_numbers);
		DeleteObject(bmp_points);
        
		DeleteDC(hCompatibleDC);
		ReleaseDC(hWnd,hDC);

		WriteInfo();

		#if defined LINESlog
		fclose(log);
		#endif

		PostQuitMessage(0);
		break;
// ��������� ��������� �� ���������
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// ������� ������� ���� ������� About
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG:
				return TRUE;

		case WM_COMMAND:
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
			{
				EndDialog(hDlg, LOWORD(wParam));
				return TRUE;
			}
			break;
	}
    return FALSE;
}

// ������� ������� ���� ������� Custom
LRESULT CALLBACK Custom(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static char szVal[10];

	switch (message)
	{
		case WM_INITDIALOG:
			ostrstream(szVal,sizeof(szVal)) << max_x << ends;
			SendDlgItemMessage(hDlg, IDC_EDIT1, WM_SETTEXT, (WPARAM)0,(LPARAM)szVal);
			ostrstream(szVal,sizeof(szVal)) << max_y << ends;
			SendDlgItemMessage(hDlg, IDC_EDIT2, WM_SETTEXT, (WPARAM)0,(LPARAM)szVal);
			ostrstream(szVal,sizeof(szVal)) << app_balls << ends;
			SendDlgItemMessage(hDlg, IDC_EDIT3, WM_SETTEXT, (WPARAM)0,(LPARAM)szVal);
			ostrstream(szVal,sizeof(szVal)) << del_balls << ends;
			SendDlgItemMessage(hDlg, IDC_EDIT4, WM_SETTEXT, (WPARAM)0,(LPARAM)szVal);
			return TRUE;

		case WM_COMMAND:
			if (LOWORD(wParam) == IDOK) 
			{
				SendDlgItemMessage(hDlg, IDC_EDIT1, WM_GETTEXT, (WPARAM)sizeof(szVal),(LPARAM)szVal);
				istrstream(szVal,sizeof(szVal)) >> max_x;
				SendDlgItemMessage(hDlg, IDC_EDIT2, WM_GETTEXT, (WPARAM)sizeof(szVal),(LPARAM)szVal);
				istrstream(szVal,sizeof(szVal)) >> max_y;
				SendDlgItemMessage(hDlg, IDC_EDIT3, WM_GETTEXT, (WPARAM)sizeof(szVal),(LPARAM)szVal);
				istrstream(szVal,sizeof(szVal)) >> app_balls;
				SendDlgItemMessage(hDlg, IDC_EDIT4, WM_GETTEXT, (WPARAM)sizeof(szVal),(LPARAM)szVal);
				istrstream(szVal,sizeof(szVal)) >> del_balls;
				EndDialog(hDlg, LOWORD(wParam));
			}
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
			{
				EndDialog(hDlg, LOWORD(wParam));
				return TRUE;
			}
			break;
	}
    return FALSE;
}

// ������� ������� ���� ������� BestResult
LRESULT CALLBACK BestResults(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static char szVal[50];
	static int i, h ,m1,m2,s1,s2;

	switch (message)
	{
		case WM_INITDIALOG:
			for (i=0;i<3;i++)
			{
				h=leaders[i].time;
				s2=h%60;
                h/=60;
				s1=s2%10; 
                s2/=10;
				m2=h%60;
                h/=60;
				m1=m2%10;
                m2/=10;
				for (int k=0;k<sizeof(szVal);++k)
                {
                    szVal[k]=leaders[i].name[k];
                }
				SendDlgItemMessage(hDlg, IDC_EDIT1+i, WM_SETTEXT, (WPARAM)0,(LPARAM)szVal);
				ostrstream(szVal,sizeof(szVal))<< leaders[i].score<<"   "<<h<<':'<<m2<<m1<<':'<<s2<<s1<< ends;
				SendDlgItemMessage(hDlg, IDC_EDIT4+i, WM_SETTEXT, (WPARAM)0,(LPARAM)szVal);
			}
			return TRUE;

		case WM_COMMAND:
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
			{
				EndDialog(hDlg, LOWORD(wParam));
				return TRUE;
			}
			break;
	}
    return FALSE;
}
// ������� ������� ���� ������� GetName, �� ���� �����������, ������ � ���������� ����� ������� ������ ���-�� �����. ��� ���� - ���...
LRESULT CALLBACK GetName(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static char szVal[30];

	switch (message)
	{
		case WM_INITDIALOG:
				return TRUE;

		case WM_COMMAND:
			if (LOWORD(wParam) == IDOK) 
			{
				SendDlgItemMessage(hDlg, IDC_EDIT1, WM_GETTEXT, (WPARAM)sizeof(szVal),(LPARAM)szVal);
                MessageBox(NULL, szVal, "LEADER", MB_OK);//��� ����� �������� ���������. �������, ��� ���� ���������, ������ �������� � ����-�� �� �����???
                /*������� SendDlgItemMessage ���������� ��������� ��������� �������� ���������� � ���������� ����.

���������

LONG SendDlgItemMessage
(
	HWND hDlg,		// ���������� ����������� ����
	int nIDDlgItem,	// ������������� �������� ����������
	UINT Msg,		// ��������� ��� ��������
	WPARAM wParam,		// ������ �������� ���������
	LPARAM lParam 		// ������ �������� ���������
);

                ���������
hDlg
�������������� ���������� ����, ������� �������� ����� ����������.
nIDDlgItem
���������� ������������� ������ ����������, ������� ��������� ���������.
Msg
���������� ���������, ������� ����� ����������.
wParam
���������� �������������� ���������������� ��������� ����������.
lParam
���������� �������������� ���������������� ��������� ����������.

*/
                //ostrstream(leaders[gametype].name,sizeof(szVal)) << szVal << ends;//������ ����� �������� ������ ��� ���������� ����� ������������, ��������� ������������ �� ������������������ ��������, � �����
                //_write( , szVal, sizeof(szVal);
                for (int i=0;i<sizeof(szVal);++i)
                {
                    leaders[gametype].name[i]=szVal[i];
                }
                MessageBox(NULL, leaders[gametype].name, "LEADER", MB_OK);
                
                /*ostrstream(
   char *_Ptr, 
   streamsize _Count,
   ios_base::openmode _Mode = ios_base::out
);

���������

_Ptr

    �����.
_Count

    ������ ������ � ������.
_Mode

    ����� ����� � ������ ������. */
                
			}
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
			{
				EndDialog(hDlg, LOWORD(wParam));
                /*������� EndDialog ��������� ��������� ���������� ����, �������� ������� ��������� ����� ��������� ��� ����� �������.

���������

BOOL EndDialog
(
	HWND hDlg,	// ���������� ����������� ����
	int nResult	// ������������ ��������
);

���������
hDlg
�������������� ���������� ����, ������� ����� ���������.
nResult
������������� ��������, ������� ����� ���������� ���������� ��������� �� �������, ������� ��������� ���������� ����.

������������ ��������
���� ������� ����������� �������, ������������ �������� ������� �� ����.
���� ������� �� ��������� ������, ������������ �������� �������.*/
				return TRUE;
			}

			break;
	}
    return FALSE;
}

//������� "���������� �����"
void ALines(int e)
{
	int y_old = y_lines;

	switch(y_lines)
	{
	//����� �������� ����
	case 0:
		if (e==0 && Cell_with_ball() )		
        { 
            Start_jumping();
            y_lines=1;
        }
		break;
	//����� ���� ������� ������� ��� 
	case 1:
		if (e==0 && Empty_cell() && Have_path() )
        {
            End_jumping();
            Start_moving();
            y_lines=2;

        }
		else 
		if (e==0 && Cell_with_ball() )	
        { 
            End_jumping();
            Start_jumping();
            Jumping();
        }
		else
		if (e==0 && Cell_with_jumping_ball() )	
        { 
            End_jumping();		
            y_lines=0;
        }
		else
		if (e==1)		
        {
            Jumping(); 
        }
		break;
	//������������ �������� ����
	case 2:
		if	(e==1 && Moving_finished() && Del_line_after_moving() )	
        { 
            End_moving(); 
            Start_delete();
            y_lines=3;
        }
		else if (e==1 && Moving_finished() )		
        {
            End_moving();
            Start_appearing();
            y_lines=4;
        }
		else if (e==1)
        {
            Move_next();
        }
		break;
	//�������� �����
	case 3:
		if (e==1 && Del_finished() )	
        { 
            End_delete();	
            y_lines=0;
        }
		else if (e==1)	
        { 
            Deleting(); 
        }
		break;
	//��������� ����� �����
	case 4:
		if (e==1 && Appear_finished() && Del_line_after_appearing() )
        { 
            End_appear();
            Start_delete();
            y_lines=3;
        }
		else if (e==1 && Appear_finished() )	
        { 
            End_appear();	
            y_lines=0;
        }
		else if (e==1)	
        {
            Appearing();
        }
		break;
	}

	#if defined LINESlog
	if (y_lines!=y_old)
	{
		char s[30];
		time_t t;
		time(&t);
		strftime(s,30,"%X", gmtime(&t));
		fprintf(log,"[%s] ������� ���������� ����� �� ��������� \"%s\"\n ������� � ��������� \"%s\".\n",s,aLines_states[y_old],aLines_states[y_lines]);
	}
	#endif
	
	if (y_old!=y_lines)
	switch(y_lines)
	{
	case 1:
		Jumping();
		break;
	case 2:
		Move_next();
		break;
	case 3:
		Deleting();
		break;
	case 4:
		Appearing();
		break;
	}
}

//������� ����������

//��������� ������ �����
bool Empty_cell()
{
	return (click_ball.State()==0 || click_ball.State()==1);
}
//� ��������� ������ ��������� ���
bool Cell_with_ball()
{
	return (click_ball.State()==3);
}
//� ��������� ������ ��������� ��������� ���
bool Cell_with_jumping_ball()
{
	return (click_ball.State()==4);
}

//���������� ���� �� �������� ������ �� ���������
bool Have_path()
{
	return FindPath(ball,click_ball);
}
//������������ �����������
bool Moving_finished()
{
	return path.empty();
}
//��������� ������� ����� (����� ������������)
bool Del_line_after_moving()
{
	return CheckLines(ball);
}
//�������� �����������
bool Del_finished()
{
	itr=explode_list.begin();
	return (itr->State()==0);//N3
}
//��������� �����������
bool Appear_finished()
{
	itr=appear_list.begin();
	return (itr->State()==3);//N1
}
//��������� ������� ����� (����� ��������� �����)
bool Del_line_after_appearing()
{
	itr=appear_list.begin();
	while (itr!=appear_list.end())
		CheckLines(*itr++);

	return (explode_list.size()!=0);
}

// �������� �����������

//��������� ������
void Start_jumping()
{
	SetTimer(hWnd,1,50,NULL);
	ball=click_ball;
}
//��������� ������������
void Start_moving()
{
	SetTimer(hWnd,1,50,NULL);
	ball_color=ball.Color();
}
//��������� ������
void End_jumping()
{
	KillTimer(hWnd,1);
	ball.ACell(6);
}
//�������
void Jumping()
{
	ball.ACell(5);
}
//����������� ��� �� ��������� ������
void Move_next()
{
	ball.ACell(0);
	ball=path.top();
	path.pop();
	ball.ACell(1);
}
//��������� ������������
void End_moving()
{
	KillTimer(hWnd,1);
}
//���������  ��������
void Start_delete()
{
	SetTimer(hWnd,1,20,NULL);
}
//��������� ���������
void Start_appearing()
{
	CheckAppearList();
	SetTimer(hWnd,1,50,NULL);
}
//�������
void Deleting()
{
	itr=explode_list.begin();
	while (itr!=explode_list.end())
		(*itr++).ACell(4);
}
//��������� ��������
void End_delete()
{
	KillTimer(hWnd,1);
    gamescore += (explode_list.size() - del_balls + 1) * explode_list.size();//����� ���������� ������� �����
    DrawScore();
    if (explode_list.size()>5)
    {
       // MessageBox(NULL, "EXAMPLE!!!", "FIND LINE", MB_OK);
        //new_Joker_appear;
        joker=joker+1;

    }
	explode_list.clear(); 
}
//���������
void Appearing()
{
	itr=appear_list.begin();
	while (itr!=appear_list.end())
		(*itr++).ACell(3);
}
//��������� ���������
void End_appear()
{
	KillTimer(hWnd,1);
	GenerateAppearList();
}
bool FindLines(int x, int y)
{
    int i,j;
    int colour;
    colour=map[x][y].color;
	Cell l, p;
    p.posx=x;
    p.posy=y;
    bool b=false;
    i=1;
    while ((x+i<max_x)&&(map[x+i][y].y == 3)&&((map[x+i][y].color == colour)||(map[x+i][y].color == 7)))
    {
        ++i;
    }
	j=1;
    while((x-j>=0)&&(map[x-j][y].y == 3)&&((map[x-j][y].color == colour)||(map[x-j][y].color == 7)))
    {
        ++j;
    }
	if (j+i-1>=del_balls)//���� ���������� ����������� ����� ��� ����� � ������ ������� � ����� �� ����� �������� ��� ��������
	{
		l.posx=x+i;
		l.posy=y;
		for(int k=0;k<i+j-1;k++)
		{
			l.posx--;
			explode_list.push_back(l);
		}
		b=true;
	}
    //����� ����� �� ���������
	i=1;
    while ((y+i<max_y)&&(map[x][y+i].y == 3)&&((map[x][y+i].color == colour)||(map[x][y+i].color == 7)))
        ++i;
	j=1;
    while((y-j>=0)&&(map[x][y-j].y == 3)&&((map[x][y-j].color == colour)||(map[x][y-j].color == 7)))
        ++j;
	if (j+i-1>=del_balls)
	{
		l.posx=x;
		l.posy=y+i;
        
       for(int k=0;k<i+j-1;k++)
		{
			l.posy--;
			explode_list.push_back(l);
		}
		b=true;
        
	}
    
    //����� ����� �� ��������� ����� �������
	i=1;
    while ((x+i<max_x)&&(y+i<max_y)&&(map[x+i][y+i].y == 3)&&((map[x+i][y+i].color == colour)||(map[x+i][y+i].color == 7)))
        ++i;
	j=1;
    while((x-j>=0)&&(y-j>=0)&&(map[x-j][y-j].y == 3)&&((map[x-j][y-j].color == colour)||(map[x-j][y-j].color == 7)))
        ++j;
	if (j+i-1>=del_balls)
	{
		l.posx=x+i;
		l.posy=y+i;
		for(int k=0;k<i+j-1;k++)
		{
			l.posx--;
			l.posy--;
			explode_list.push_back(l);
		}
		b=true;
	}
    //����� ����� �� ��������� ������ ������
	i=1;
    while ((x+i<max_x)&&(y-i>=0)&&(map[x+i][y-i].y == 3)&&((map[x+i][y-i].color == colour)||(map[x+i][y-i].color == 7)))
        ++i;
	j=1;
    while((x-j>=0)&&(y+j<max_y)&&(map[x-j][y+j].y == 3)&&((map[x-j][y+j].color == colour)||(map[x-j][y+j].color == 7)))
        ++j;
	if (j+i-1>=del_balls)
	{
		l.posx=x+i;
		l.posy=y-i;
		for(int k=0;k<i+j-1;k++)
		{
			l.posx--;
			l.posy++;
			explode_list.push_back(l);
		}
		b=true;
	}
    if (b) 
	{
		explode_list.remove(p);
		explode_list.push_back(p);
        return b;
        //MessageBox(NULL, "EXAMPLE!!!", "FIND LINE", MB_OK);
	}
	return b;

}

void Find_in_head(int &x, int &y, int trend)
{
    int x1=x;
    int y1=y;
    int local_x=x;
    int local_y=y;
    
    int counter=0;
    int counter1=0;
    switch (trend)
    {
    case 0: //����� ����� - �������� ����������� ���� ��� ����� ����� �������, ���� ������� ������� - ��� ������� �����
        while(map[local_x][local_y].color==7)
            {
                if(((local_x>0)&&(local_y<max_y))&&(map[local_x][local_y].color==7))
                {
                    local_x--;
                    local_y++;
                    counter++;
                } 
            }
            x1=local_x;//������ �� ���������� ������� � �����
            y1=local_y;
            while(map[local_x][local_y].color==7)
                {
                    if(((local_x<max_x)&&(local_y>0))&&(map[local_x][local_y].color==7))
                    {
                        local_x++;
                        local_y--;
                    }
                }
                //����� ������� ������� � �����
             if((local_x<max_x)&&(local_y>0))
                    {
                       x=local_x+1;
                       y=local_y-1; 
                       MessageBox(NULL, "<-Vniz","last",MB_OK);
                       return;
                       
                    }
             else if ((local_x>0)&&(local_y<max_y))
               {
                   x=x1-1;
                   y=y1+1;
                   MessageBox(NULL, "<-Vverh","last",MB_OK);
                   return;
               }
          break;
    case 1: //����� ����� - �������� ����������� ���� ��� ����� ������ ������, ���� ������� ������� - ��� ������� �����
        for(int i=0; i<del_balls-1;++i)
            {
                if(((local_x<max_x)&&(local_y<max_y))&&(map[local_x][local_y].color==7))
                {
                    local_x++;
                    local_y++;
                } 
            }
            x1=local_x;//������ �� ���������� ������� � �����
            y1=local_y;
            for(int i=0; i<del_balls-1;++i)
                {
                    if(((local_x>0)&&(local_y>0))&&(map[local_x][local_y].color==7))
                    {
                        local_x--;
                        local_y--;
                    }
                }
                //����� ������� ������� � �����
             if((local_x>0)&&(local_y>0))
                    {
                       x=local_x-1;
                       y=local_y-1; 
                    }
             else if ((local_x<max_x)&&(local_y<max_y))
               {
                   x=x1+1;
                   y=y1+1;
               }
          break;
        case 2: //����� ����� - �������� ����������� ���� ��� ����� �� �����������, ���� ������� ������� - ��� ������� �����
        for(int i=0; i<del_balls-1;++i)
            {
                if((local_y<max_y)&&(map[x][local_y].color==7))
                {
                    local_y++;
                } 
            }
            //������ �� ���������� ������� � �����
            y1=local_y;
            for(int i=0; i<del_balls-1;++i)
                {
                    if((local_y>0)&&(map[x][local_y].color==7))
                    {
                        local_y--;
                    }
                }
                //����� ������� ������� � �����
             if(local_y>0)
                    {
                      y=local_y-1; 
                    }
             else if (local_y<max_y)
               {
                  y=y1+1;
               }
          break;
        case 3: //����� ����� - �������� ����������� ���� ��� ����� �� ���������, ���� ������� ������� - ��� ������� �����
        for(int i=0; i<del_balls-1;++i)
            {
                if((local_x<max_x)&&(map[local_x][y].color==7))
                {
                    local_x++;
                } 
            }
            x1=local_x;//������ �� ���������� ������� � �����
            for(int i=0; i<del_balls-1;++i)
                {
                    if((local_x>0)&&(map[local_x][y].color==7))
                    {
                        local_x--;
                    }
                }
                //����� ������� ������� � �����
             if(local_x>0)
                    {
                       x=local_x-1; 
                    }
             else if (local_x<max_x)
               {
                   x=x1+1;
               }
          break;
        }
        }
//�������� ��� ����� ������ in �������� ���������� �������� �����(��� ��������� �����)
bool CheckLines(const Cell &in)
{
	int x=in.posx;
	int y=in.posy;
    int flag;
	int colour=in.Color();//���� ���� � ������
	int i,j;
	Cell l;
	bool b=false;
    //���� �� ������ � ������� � ����� �����, ����� �������, �� ��� � �������, ����� ���������� �����������
    //����� ����� ��� ���� �������� �����
    if (colour!=7)//���� �� ������ � ������� �� ������, � ������� �� ����� ������
    {
       b=FindLines(x,y);
       return b;
    
    }
    else//���� �� ������ � ������� �� ������, � ������� ����� ������, ��� ���������� ���������� �� ���� ������ � �������� �����
        //����� ������� - [x-1,y-1]; ������ ������ - [x+1, y-1]; ����������� - [x-1,y] � ��������� - [x, y-1]
        
    {
        
       if((x>0) &&(x<max_x))
        {
            x=x-1;
        }
        if((y>0)&&(y<max_y))
        {
             y=y-1;
             
        }
        int localy=y;
        for (int k=0; k<3; ++k)
        {
            x=x+k;
            for (int m=0; m<3; ++m)
            {
               localy=localy+m;
               b=FindLines(x,localy);
               if(b)
               {
                   return b;
               }
            }
            localy=y;
        }
        /*for (int i=0; i<4; ++i)
        {
            Find_in_head(x,y,i);
            b=FindLines(x,y);
            if(b)
                   {
                       return b;
                   }
            
        }*/
    }
    return b;
}

//����� ���� ���������� ������ �� ������ from � in
bool FindPath(const Cell &from, const Cell &in)
{
	struct
	{
		Cell pred;
		int mark;
	} 
	v[MAX_MAP_X][MAX_MAP_Y];
	
	Cell k,l;

	std::queue<Cell> q;

	for (int i=0;i<max_x;i++)
	for(int j=0;j<max_y;j++)
		v[i][j].mark = 0;

	v[from.posx][from.posy].mark=1;
	q.push(from);

	while (!q.empty())
	{
		k=q.front();
		for (int i=0;i<4;i++)
		{
			l=k;
			switch(i)
			{
			case 0:
				l.posx--;
				break;
			case 1:
				l.posx++;
				break;
			case 2:
				l.posy--;
				break;
			case 3:
				l.posy++;
				break;
			}
			if (Valid(l) && !v[l.posx][l.posy].mark)
			{
				v[l.posx][l.posy].mark=1;
				v[l.posx][l.posy].pred=k;
				q.push(l);

				if (l==in)
				{
					do
					{
						path.push(l);
						l = v[l.posx][l.posy].pred;

					} while (l!=from);
					return true;
				}
			}
		}

		q.pop();

	}

	return false;
}

// ��������� ������ ������������ ����� �� �������� ��������� ��������������� ������
void CheckAppearList()
{
	int tmp;
	itr=appear_list.begin();
	while (itr!=appear_list.end())
	{
		if (itr->State()==3)
		{
			tmp=itr->PreColor();
			itr->PreColor()=-1;
			FindEmptyCell(*itr);
			itr->PreColor()=tmp;
			(*itr).ACell(2);
		}
		++itr;
	}

}

//������� ������ ������������ �����
void GenerateAppearList()
{
	appear_list.clear();
	Cell l;
	for(int i=0;i<app_balls;i++)
		if (FindEmptyCell(l))
		{
			appear_list.push_back(l);//������� � ����� ������ ������������ ����� ������ l
            
			l.ACell(2);//������� "������" ������� � ��������� "������� ���������"

           // MessageBox(NULL, "EXAMPLE!!!", "FIND Empty cell", MB_OK);
		}
		else return;//������, �� �������� ��������� ���� ��� ���������� ����� �����

}

//����� ������ ������
bool FindEmptyCell(Cell &in)
{
	Cell l;
	l.posx=random(max_x);
	l.posy=random(max_y);

	if (l.State()==0)
	{
		in=l;
		return true;
	};
	
	for (int i=0;i<max_x*max_y;i++)
	{
		if (l.posx!=max_x-1) l.posx++;
		else if (l.posy!=max_y-1) {l.posy++;l.posx=0;}
			else {l.posx=0;l.posy=0;};

		if (l.State()==0)
		{
			in=l;
			return true;
		}
	}

	GameOver();
	return false;
}

//��������� ����� �� ����� ������ in �������� ����
bool Valid(const Cell &in)
{
	return (in.posx >= 0) && (in.posx < max_x) && (in.posy >= 0) && (in.posy < max_y) && (in.State() == 0 || in.State() == 1);
}

//���������� �������� ��� ����� ���� 
void NewGame()
{
	for (int i=0;i<max_x;i++)
	for(int j=0;j<max_y;j++)
	{
		map[i][j].y=0;
		map[i][j].color=0;
		map[i][j].pre_color=-1;
		map[i][j].num_pic=0;
	}

    joker=0;
	y_lines =0;

	gamescore = 0;
	gametime = 0;

	#if defined LINESlog
	char s[30];
	time_t t;
	time(&t);
	strftime(s,30,"%X", gmtime(&t));
	fprintf(log,"[%s] ����� ����\n",s);
	#endif

	SetTimer(hWnd,0,1000,NULL);

	Cell l;

	for(int i=0;i<del_balls;i++)
	{
		FindEmptyCell(l);
		ball_color=random(7);
		l.ACell(1);
	}
	GenerateAppearList();

}

//��������� ��������� �������� ��������
void GameOver()
{
	KillTimer(hWnd,0);

	#if defined LINESlog
	char s[30];
	time_t t;
	time(&t);
	strftime(s,30,"%X", gmtime(&t));
	fprintf(log,"[%s] ���� ���������\n",s);
	#endif

	if (gametype<3)
	{
		if ((leaders[gametype].score<gamescore )|| (leaders[gametype].score==gamescore && leaders[gametype].time>gametime))
		{
			DialogBox(hInst, (LPCTSTR)IDD_GETNAMEBOX, hWnd, (DLGPROC)GetName);
			leaders[gametype].score=gamescore;
			leaders[gametype].time=gametime;
		}
		else DialogBox(hInst, (LPCTSTR)IDD_BESTRESULTSBOX, hWnd, (DLGPROC)BestResults);
	} else MessageBox(hWnd,"Your custom game is over...", "Condolences",MB_OK);
	NewGame();
	InvalidateRect(hWnd,NULL,FALSE);
}
//���������� �� ����� ����������������� ����
void DrawTime()
{
	SelectObject(hCompatibleDC, bmp_numbers);
	int h=gametime;
	int s2=h%60; h/=60;
	int s1=s2%10; s2/=10;
	int m2=h%60; h/=60;
	int m1=m2%10; m2/=10;
	BitBlt(hDC,max_x*CELL_SIZE-29,5,20,37,hCompatibleDC,s1*19,0,SRCCOPY);
	BitBlt(hDC,max_x*CELL_SIZE-50,5,20,37,hCompatibleDC,s2*19,0,SRCCOPY);
	BitBlt(hDC,max_x*CELL_SIZE-77,5,20,37,hCompatibleDC,m1*19,0,SRCCOPY);
	BitBlt(hDC,max_x*CELL_SIZE-98,5,20,37,hCompatibleDC,m2*19,0,SRCCOPY);
	BitBlt(hDC,max_x*CELL_SIZE-125,5,20,37,hCompatibleDC,h*19,0,SRCCOPY);
	SelectObject(hCompatibleDC, bmp_points);
	BitBlt(hDC,max_x*CELL_SIZE-56, 5,5,37,hCompatibleDC,0,0,SRCCOPY);
	BitBlt(hDC,max_x*CELL_SIZE-104, 5,5,37,hCompatibleDC,0,0,SRCCOPY);
}
//���������� �� ����� ������� ����
void DrawScore()
{
	SelectObject(hCompatibleDC, bmp_numbers);
	int t=gamescore;
	for (int i=0; i<5; i++)
	{
		BitBlt(hDC,100-21*i,5, 20, 37, hCompatibleDC, (t%10)*19, 0, SRCCOPY);
		t /=10;
	}
}
//���������� �����
void DrawTop()
{
	PatBlt(hDC,0, 0, 46*max_x, 46, BLACKNESS);
    /*BOOL PatBlt( HDC hdc, int x, int y, int nWidth, int nHeight, DWORD dwRop );

������������ ��������
������� �� ����, ���� ������� �������, ����� 0.

���������
x
���������� ���������� x-���������� ������ �������� ���� ��������������, ������� ������ �������� �������.
y
���������� ���������� y-���������� ������ �������� ���� ��������������, ������� ������ �������� �������.
nWidth
���������� ������ (� ���������� �������) ��������������, ������� ������ �������� �������.
nHeight
���������� ������ (� ���������� �������) ��������������, ������� ������ �������� �������.
dwRop
���������� ��� ��������� ���������. ���� ��������� ��������� (ROPS) ����������, ��� GDI ���������� ����� � ��������� ������, ������� �������� ������� �����, ��������� �������� �������� �������, � �������� ������� ��������. ���� �������� ����� ���� ���� �� ��������� ��������:

    PATCOPY - ����� �������� � ��������� ������� ��������.
    PATINVERT - ���������� �������� ������� �������� � ��������, � �������������� ������� ��������� XOR.
    DSTINVERT - ����������� �������� ������� ��������.
    BLACKNESS - ���������� ���� ������ �����.
    WHITENESS - ���������� ���� ����� �����. */
	DrawScore();
	DrawTime();
}
//��������� �������� ��������� ����
void CheckCustomParameters()
{
	if (max_x<6) max_x=6;
	if (max_x>20) max_x=20;
	if (max_y<2) max_y=2;
	if (max_y>12) max_y=12;

	if (del_balls<2) del_balls=2;

	if (del_balls>(max_x>max_y?max_x:max_y)) del_balls = (max_x>max_y?max_x:max_y);
	
	if (app_balls+del_balls > max_x*max_y)
	{
		app_balls=max_x*max_y-del_balls;
	}
}
//��������� ���������� � ������ �������
void GetInfo()
{
	FILE *in;
	if (in = fopen("leaders.dat","r"))
	{
		fread(&gametype,sizeof(int),1,in);
		fread(&max_x,sizeof(int),1,in);
		fread(&max_y,sizeof(int),1,in);
		fread(&app_balls,sizeof(int),1,in);
		fread(&del_balls,sizeof(int),1,in);
		for (int i=0;i<3;i++)
			fread(&leaders[i],sizeof(info),1,in);
		fclose(in);
	}
	else
	{
		gametype=0;
		max_x=9;max_y=9;
		app_balls=3;del_balls=5;
		for (int i=0;i<3;i++)
			leaders[i]=null_leader;
	}
}
//��������� ���������� � ������ �������
void WriteInfo()
{
	FILE *out;
	if (out = fopen("leaders.dat","w"))
	{
		fwrite(&gametype,sizeof(int),1,out);
		fwrite(&max_x,sizeof(int),1,out);
		fwrite(&max_y,sizeof(int),1,out);
		fwrite(&app_balls,sizeof(int),1,out);
		fwrite(&del_balls,sizeof(int),1,out);
		for (int i=0;i<3;i++)
			fwrite(&leaders[i],sizeof(info),1,out);
		fclose(out);

	}
}
